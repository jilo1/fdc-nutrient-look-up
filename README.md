## Introduction

'fdc-nutrient-loop-up.py' is a Python script that performs a nutrient lookup for food items using the FoodData Central (FDC) database by the US Department of Agriculture (USDA). The script reads data from a Google Sheets spreadsheet and performs nutrient lookups for each food item's unique FDC ID. It then generates HTML tables for each food item's nutrient information and saves the results to CSV files and a new sheet named "NutritionTable" in the same Google Sheets spreadsheet.

## Prerequisites
Before using this script, make sure you have the following:
1. Python 3 installed on your system.
2. Required Python packages: pandas, gspread, oauth2client, and gspread_dataframe. You can install these packages using pip:
 - pip install pandas gspread oauth2client gspread_dataframe

3. A Google Cloud Platform (GCP) service account key JSON file. This file contains the necessary credentials to access Google Sheets and should be named celestial-now-393101-ac7c4fd2a26c.json. You can create a service account and download the JSON key file from the GCP Console.
4. The two CSV files food_nutrient.csv and nutrients_with_priority_and_pruned-v3.csv, which contain the nutrient data. Place these files in the same directory as the script.

## Usage
1. Ensure that you have all the prerequisites mentioned above.
2. Share the Google Sheets spreadsheet with the service account email terma-project@celestial-now-393101.iam.gserviceaccount.com as an Editor. This allows the script to access and write to the spreadsheet.
3. Run the script using Python:
 - python fdc-nutrient-loop-up.py

    1. The script will prompt you to enter the URL of the Google Sheets spreadsheet you want to work with. Copy the URL from the browser and paste it when prompted.
    2. The script will read the CSV files (food_nutrient.csv and nutrients_with_priority_and_pruned-v3.csv) and display their contents.
    3. Next, the script will create a new folder named "results" in the same directory as the script to save the generated output.
    4. The script will iterate through each row in the selected sheet of the Google Sheets spreadsheet and perform nutrient lookups for each food item using the FDC database.
    5. For each food item with a valid FDC ID, the script will create an HTML table containing nutrient information and save the results to individual CSV files.
    6. All the individual nutrient tables will then be combined into a single DataFrame and saved to a CSV file named combined_results.csv.
    7. The script will also save the combined nutrient tables to a new sheet named "NutritionTable" in the Google Sheets spreadsheet.
    8. The URL of the "NutritionTable" sheet will be displayed as output.

## Notes
• Make sure that the food_nutrient.csv and nutrients_with_priority_and_pruned-v3.csv files contain the required nutrient data in the correct format.

• Ensure that the Google Sheets spreadsheet contains a sheet with the specified name where the "fdc_id" column exists.

• If a food item's FDC ID is not found or invalid, no nutrient lookup will be performed for that item, and the script will display a message indicating that no data was found for that FDC ID.

• The script will create or use an existing folder named "results" in the same directory to store the generated CSV files.

• Before running the script, ensure that you have shared the Google Sheets spreadsheet with the service account email as an Editor, as mentioned in Step 2 of the Usage section.

• The script will also print intermediate DataFrame results for each food item's nutrient data and generated HTML tables for verification purposes.

• The HTML tables are generated with nutrient information sorted by priority in descending order (highest to lowest), excluding rows with zero amount values.

• For nutrients with units in "kcal," the script will convert the amount values to "cal" and update the unit name accordingly.

• The "NutritionTable" sheet in the Google Sheets spreadsheet will contain the combined nutrient tables with HTML code for each food item's nutrient information.

• The script has error handling to handle various exceptions that might occur during the execution.

• Please ensure that you have a stable internet connection to access Google Sheets and the FDC database.

• The script is intended to be used for educational and research purposes and not for commercial applications.

• Extract results.zip file.

## Disclaimer
The author is not responsible for any misuse, data loss, or damages caused by using this script. Use it at your own risk.

