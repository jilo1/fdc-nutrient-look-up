import pandas as pd
import gspread, os
from oauth2client.service_account import ServiceAccountCredentials
import re
import gspread_dataframe as gsdf

print("\nIf the Spreadsheet you want to work with is not available then share the Email to that Spreadsheet as Editor and re-run this Pipeline\nEmail: terma-project@celestial-now-393101.iam.gserviceaccount.com")
credentials_path = os.path.join(os.path.dirname(__file__), 'celestial-now-393101-ac7c4fd2a26c.json')

# Set up the credentials
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name(credentials_path, scope)
client = gspread.authorize(credentials)

prompt_versions_googlesheet_url = "https://docs.google.com/spreadsheets/d/1iiUzmWWCuNSVf8ewqQmIpJpLFpur6yD245b_If98gSg/edit#gid=0"

pvpct = client.open_by_url(prompt_versions_googlesheet_url)

# Ask user to input a sheet name
version_sheet = "Versions"

# Open the selected sheet
pvpct_sheet = pvpct.worksheet(version_sheet)

# Get all values from the sheet
pvpct_data = pvpct_sheet.get_all_values()

# Convert the data to a Pandas DataFrame
pvpct_df = pd.DataFrame(pvpct_data[1:], columns=pvpct_data[0])
filtered_df = pvpct_df[pvpct_df['Run Table Gen / Link Injection'].str.lower() == 'yes']
print("Filtered DataFrame:", filtered_df)

for index, row in filtered_df.iterrows():
    # Access data in each row using column names
    spreadsheet_url = row['Link to Input Prompt Tables']
    version_num = row["Version Number"]

    # Now you can perform operations on the 'Run Table Gen / Link Injection' column
    print(f'Spreadsheet Row {index+2}: Run "Table Gen / Link Injection" on this Link: {spreadsheet_url}\nVersion Number: {version_num}')

    # Read the CSV files into DataFrames
    food_nutrient_df = pd.read_csv(os.path.join(os.path.dirname(__file__), "food_nutrient.csv"))
    nutrient_df = pd.read_csv(os.path.join(os.path.dirname(__file__), "nutrients_with_priority_and_pruned-v6.csv"))

    # Print the DataFrames
    print("food_nutrient.csv DataFrame:")
    print(food_nutrient_df)

    print("\nnutrient.csv DataFrame:")
    print(nutrient_df)

    # Create a new folder named "results" in the same directory as the script
    current_directory = os.path.dirname(os.path.abspath(__file__))
    new_folder_path = os.path.join(current_directory, "results")

    # Function to generate HTML message
    def generate_html_message(row):
        fdc_caption = row["Other FDC Name"]
        fdc_food_item = row['Keyword (food item)']
        print("Other FDC Name:",fdc_caption)

        if fdc_caption == "":  # Check if fdc_caption exists (not NaN)
            # Message if fdc_caption exists
            message = (
                f"Nutritional data is sourced from the US Department of Agriculture's <a href='https://fdc.nal.usda.gov/fdc-app.html#/food-details/{fdc_id_value}/nutrients' target='_blank'>FoodData Central system</a>. Please see <a href='#cast-iron-ketos-editorial-and-research-standards'>Cast Iron Keto's editorial and research standards</a> for more information."
            )
        else:
            # Message if fdc_caption does not exist (is NaN)6
            message = (
                f"This data was provided by the US Department of Agriculture's <a href='https://fdc.nal.usda.gov/fdc-app.html#/food-details/{fdc_id_value}/nutrients' target='_blank'>FoodData Central system</a>.<br>"
                f"'{fdc_food_item}' was not found in FoodData Central, so nutritional data for '{fdc_caption}' was used instead under <a href='#cast-iron-ketos-editorial-and-research-standards'>Cast Iron Keto's editorial and research standards</a>."
            )
        
        return message

    try:
        # Open the spreadsheet using the provided URL
        spreadsheet = client.open_by_url(spreadsheet_url)
        
        # Print the name and URL of the spreadsheet
        print(f"\nSpreadsheet Name: {spreadsheet.title}")
        print(f"Spreadsheet URL: {spreadsheet.url}")

        sheet_name_input = "Keywords"

        # Open the selected sheet
        sheet = spreadsheet.worksheet(sheet_name_input)

        # Get all values from the sheet
        data = sheet.get_all_values()

        # Convert the data to a Pandas DataFrame
        df = pd.DataFrame(data[1:], columns=data[0])
        df = df[["Keyword (food item)","fdc_id","Other FDC Name","Serving Size (g)"]]

        print(f"\nDataFrame for Sheet '{sheet_name_input}':")
        print(df)

        # Create a new folder named "results" in the same directory as the script
        new_folder_path = os.path.join(current_directory, "results")
        try:
            os.mkdir(new_folder_path)
            print(f"New folder 'results' created in the path: {new_folder_path}")
        except FileExistsError:
            print(f"The 'results' folder already exists in the path: {new_folder_path}")

        # Create an empty list to store all selected_column DataFrames
        all_selected_columns = []

        # Iterate through each row in the DataFrame
        for index, row in df.iterrows():
            serving_size = row["Serving Size (g)"]
            fdc_id_value = row["fdc_id"]
            # print(serving_size)
            html_message = generate_html_message(row)

            try:
                # Try to convert fdc_id_value to an integer
                fdc_id_value = int(fdc_id_value)
            except (ValueError, TypeError):
                # If it's not an integer or None, set fdc_id_value to None
                fdc_id_value = None

            # Find all rows in food_nutrient_df where fdc_id matches the value from the loop
            matching_rows = food_nutrient_df.loc[food_nutrient_df["fdc_id"] == fdc_id_value]

            # Check if there are any matching rows
            if not matching_rows.empty:
                # Create an empty DataFrame to store the extracted data for this fdc_id_value
                extracted_data = pd.DataFrame(columns=food_nutrient_df.columns)

                # Drop rows where "amount" column has zero value
                extracted_data = extracted_data[(extracted_data["amount"] != 0) & (extracted_data["amount"] != 0.0)]
                
                for _, matching_row in matching_rows.iterrows():
                    # Create a new DataFrame from matching_row
                    matching_data = pd.DataFrame([matching_row], columns=food_nutrient_df.columns)
                    
                    # Append the matching_data to extracted_data
                    extracted_data = pd.concat([extracted_data, matching_data], ignore_index=True)

                # Merge the extracted_data DataFrame with nutrient_df based on the nutrient_id column
                extracted_data = pd.merge(extracted_data, nutrient_df, left_on="nutrient_id", right_on="id", suffixes=("", "_nutrient"))

                # Convert the "unit_name" column to lowercase
                extracted_data["unit_name"] = extracted_data["unit_name"].str.lower()

                # Filter out rows with amount == 0 or amount == 0.0
                extracted_data = extracted_data[(extracted_data["amount"] != 0) & (extracted_data["amount"] != 0.0)]

                # Sort the extracted_data DataFrame by the "priority" column in descending order (Z to A)
                extracted_data = extracted_data.sort_values(by="exact_priority", ascending=True)

                # Print the sorted DataFrame
                print(f"\nDataFrame for Sheet '{fdc_id_value}' sorted by exact_priority (excluding rows with amount == 0 and kcal conversion):")
                print(extracted_data)

                # Define a function to format each row value as <td>{}</td>
                def format_as_td(value):
                    return "<td>{}</td>".format(value)

                extracted_data["Nutrient Name"] = extracted_data["name"]
                extracted_data["Amount"] = (extracted_data["amount"] * (int(serving_size)/100)).round(2)
                extracted_data["Unit Name"] = extracted_data["unit_name"]

                # Check if "Carbohydrate, by difference" and "Fiber, total dietary" exist
                if "Carbohydrate, by difference" in extracted_data["Nutrient Name"].values and \
                "Fiber, total dietary" in extracted_data["Nutrient Name"].values:

                    # Find the row where Nutrient Name is "Carbohydrate, by difference" 
                    carb_row = extracted_data[extracted_data["Nutrient Name"] == "Carbohydrate, by difference"]

                    # Check if the row exists
                    if not carb_row.empty:
                        # Extract the value from the "Amount" column
                        carb_amount = carb_row["Amount"].iloc[0]
                        print("Carb Amount:", carb_amount)
                    else:
                        carb_amount = 0
                        print("Carb row not found")

                    # Find the row where Nutrient Name is "Fiber, total dietary"
                    fiber_row = extracted_data[extracted_data["Nutrient Name"] == "Fiber, total dietary"]

                    # Check if the row exists
                    if not fiber_row.empty:
                        # Extract the value from the "Amount" column
                        fiber_amount = fiber_row["Amount"].iloc[0]
                        print("Fiber Amount:", fiber_amount)
                    else:
                        fiber_amount = 0
                        print("Fiber row not found")

                    net_carbs_value = carb_amount - fiber_amount

                    # Format the net_carbs_value to display with 2 decimal places
                    net_carbs_value = round(net_carbs_value, 2)

                    # Create a new row for "Net Carbs" in the DataFrame
                    new_row = pd.DataFrame({"Nutrient Name": ["Net Carbs"], "Amount": net_carbs_value, "Unit Name": "g"})
                    # Concatenate the new row with the existing DataFrame
                    extracted_data = pd.concat([new_row, extracted_data], ignore_index=True)

                print(extracted_data)

                # Concatenate 'Amount' and 'Unit Name' with a space and create 'Amount and Unit' column
                extracted_data[f"Amount and Unit per {serving_size} g"] = extracted_data["Amount"].astype(str) +  extracted_data["Unit Name"]

                # Apply the format_as_td function to each element in the DataFrame
                extracted_data = extracted_data[["Nutrient Name",f"Amount and Unit per {serving_size} g"]].applymap(format_as_td)


                # Concatenate "name," "amount," and "unit_name" columns to create a new column "concatenated"
                extracted_data["HTML Code"] = "<tr>" + extracted_data["Nutrient Name"] + " " + extracted_data[f"Amount and Unit per {serving_size} g"].astype(str) + "</tr>"

                # New code to concatenate HTML codes and create a new DataFrame
                html_codes_list = extracted_data["HTML Code"].tolist()

                # Concatenate the HTML codes from the list into a single string
                concatenated_html = "".join(html_codes_list)

                # Create a new DataFrame with the concatenated HTML string
                nutrition_table_df = pd.DataFrame(data=[concatenated_html], columns=["Concatenated HTML Code"])

                # Append the new DataFrame to the beginning of the existing DataFrame
                extracted_data = pd.concat([nutrition_table_df, extracted_data], ignore_index=True)
                nutrition_table_value = str(extracted_data["Concatenated HTML Code"].iloc[0])
                html_code = "<table>" + f"<thead><tr><th>Nutrient Name</th><th>Amount and Unit per {serving_size} g</th></tr></thead><tbody>" + nutrition_table_value +"</tbody></table>" + "<figcaption class='wp-element-caption'>" + html_message + "</figcaption>" 

                # Removing the 'Concatenated HTML Code' column
                if 'Concatenated HTML Code' in extracted_data.columns:
                    extracted_data.drop(columns=['Concatenated HTML Code'], inplace=True)

                # Removing the first row from the DataFrame and resetting the index
                extracted_data.drop(index=0, inplace=True)
                extracted_data.reset_index(drop=True, inplace=True)

                # The first row is now removed and the index is reset in the 'extracted_data' DataFrame
                print(f"FDC ID {fdc_id_value} Dataframe with HTML Code Values:")
                print(extracted_data)
                print("Nutrition Table Value:",html_code)

                extracted_data['Nutrition Table'] = ""
                extracted_data.at[0, 'Nutrition Table'] = html_code

                all_selected_columns.append(extracted_data.iloc[0]["Nutrition Table"])

                # Save the extracted data to a CSV file with a unique name based on fdc_id_value
                csv_file_path = os.path.join(new_folder_path, f"results-{fdc_id_value}.csv")
                extracted_data.to_csv(csv_file_path, index=False)
                print(f"Data for fdc_id {fdc_id_value} has been saved to '{csv_file_path}'")
            else:
                # Create an empty DataFrame with a "Nutrition Table" column
                extracted_data = pd.DataFrame(columns=['Nutrition Table'])

                # Add a row to the DataFrame
                extracted_data.loc[0] = "None"

                # Print the DataFrame to verify the change
                print("Extracted Dataframe:")
                print(extracted_data)

                # Save the extracted data to a CSV file with a unique name based on fdc_id_value
                csv_file_path = os.path.join(new_folder_path, f"results-none.csv")
                extracted_data.to_csv(csv_file_path, index=False)
                all_selected_columns.append(extracted_data.iloc[0]["Nutrition Table"])
                print(f"No data found for fdc_id {fdc_id_value}")

        all_selected_columns_df = pd.DataFrame({"Nutrition Table": all_selected_columns})

        # Save the combined data to a single CSV file
        combined_csv_file_path = os.path.join(new_folder_path, "combined_results.csv")
        all_selected_columns_df.to_csv(combined_csv_file_path, index=False)

        # Indicate that data is being saved to the "NutritionTable" sheet
        print("Saving Data to the Google Sheet with the sheet name 'NutritionTable'...")

        # Check if the "NutritionTable" sheet exists
        try:
            nutritiontable_sheet = spreadsheet.worksheet("NutritionTable")

            # If the sheet exists, clear all data from it
            nutritiontable_sheet.clear()

            print("Sheet 'NutritionTable' existed and has been cleaned.")
        except gspread.exceptions.WorksheetNotFound:
            # If the sheet doesn't exist, create a new one
            nutritiontable_sheet = spreadsheet.add_worksheet("NutritionTable", rows=1, cols=1)

            print("Sheet 'NutritionTable' did not exist and has been created.")

        # Save all_selected_columns_df to the "NutritionTable" sheet
        gsdf.set_with_dataframe(nutritiontable_sheet, all_selected_columns_df)

        # Print the URL of the "NutritionTable" sheet
        print(f"Data has been saved to the Google Sheet 'NutritionTable' with URL: {nutritiontable_sheet.url}")

    except gspread.exceptions.APIError as e:
        print(f"Error accessing the spreadsheet: {e}")

    except gspread.exceptions.SpreadsheetNotFound:
        print("Spreadsheet not found. Please check the URL and try again.")

    except gspread.exceptions.WorksheetNotFound:
        print("Sheet not found. Please check the sheet name and try again.")